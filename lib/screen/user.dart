import 'package:berkah_manis_makmur/bloc/bloc_login.dart';
import 'package:berkah_manis_makmur/model/model_dblogin.dart';
import 'package:flutter/material.dart';

class UserScreen extends StatefulWidget {
  const UserScreen({ Key? key }) : super(key: key);

  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {

  var bloc = LoginBloc();

  @override
  void initState() {
    super.initState();
    bloc.fetchSqliteGet(context);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('User List'),
      ),
      body: StreamBuilder(
        stream: bloc.getDbSqliteUser,
        builder: (context, AsyncSnapshot<List<ModelDbLogin>?> snapshot){
          if(snapshot.data != null){
            return listUser(snapshot.data);
          }else{
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        }
      )
    );
  }

  Widget listUser(List<ModelDbLogin>? model){
    return ListView.builder(
      itemCount: model?.length,
      itemBuilder: (context, index){
        return Container(
          padding: EdgeInsets.only(left: 20, right: 20, top: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Username : "+model![index].username.toString()),
              SizedBox(height: 5),              
              Text("Password : "+model[index].password.toString()),
              Divider()
            ],
          ),
        );
      }
    );
  }
}