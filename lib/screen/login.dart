import 'package:berkah_manis_makmur/bloc/bloc_login.dart';
import 'package:berkah_manis_makmur/screen/signup.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({ Key? key }) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  var bloc = LoginBloc();
  late Size size;
  bool isPasswordVisible = true;

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: size.width,
          height: size.height,
          padding: EdgeInsets.all(20.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                imageBox(),
                tvUsername(),
                SizedBox(height: 10,),
                tvPassword(),
                SizedBox(height: 20,),
                btnLogin(),
                SizedBox(height: 10,),
                btnSignup()
              ],
            ),
          ),
        ),
      ),   
    );
  }

  Widget imageBox(){
    return Stack(
      children: [
        Center(
          child: Padding(
            padding: EdgeInsets.all(20),
            child: Image.asset('assets/images/logo.jpg', width: 250,),
          ),
        ),                
        Image.asset('assets/images/bmm.jpg', width: 200,),
      ],
    );
  }

  Widget tvUsername(){
    return StreamBuilder<String>(
      stream: bloc.username,
      builder: (context, snapshot) {
        return TextFormField(
          keyboardType: TextInputType.text, 
          controller: bloc.txtUsernameController,         
          onChanged: bloc.setUsername,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.only(top: 20),                        
            hintText: 'Username',
            errorText: snapshot.error?.toString(),
            errorStyle: TextStyle(color: Colors.red),
            // prefixIcon: Icon(Icons.person),            
            icon: Icon(Icons.person),
          )
        );
      }
    );
  }

  Widget tvPassword(){
    return StreamBuilder<String>(
      stream: bloc.password,
      builder: (context, snapshot) {
        return TextFormField(
          keyboardType: TextInputType.text,
          controller: bloc.txtPasswordController,
          onChanged: bloc.setPassword,
          obscureText: isPasswordVisible,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.only(top: 20),                        
            hintText: 'Passord',
            errorText: snapshot.error?.toString(),
            errorStyle: TextStyle(color: Colors.red),
            suffixIcon: IconButton(
              icon: Icon(isPasswordVisible
                  ? Icons.visibility_off
                  : Icons.visibility),
              onPressed: () {
                setState(() {
                  isPasswordVisible = !isPasswordVisible;
                });
              },
            ),            
            icon: Icon(Icons.lock),
          )
        );
      }
    );
  }

  Widget btnLogin(){    
    return StreamBuilder<bool>(
      stream: bloc.loginValid,
      builder: (context, snapshot) {
        return Container(
          width: size.width,
          height: 45,
          child: ElevatedButton(
            onPressed: (){              
              snapshot.hasData ? bloc.fetchSqliteValidateLogin(context) : null;
            },
            child: Text("Login"),
            style: ElevatedButton.styleFrom(
              primary: !snapshot.hasData ? Colors.grey : Colors.red,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
              )
            ),
          ),
        );
      }
    );
  }

  Widget btnSignup(){
    return Column(
      children: [
        InkWell(
          child: Text("Blum Punya Account?"),
        ),
        SizedBox(height: 3,),
        GestureDetector(
          onTap: () {
            Navigator.push(context,
              MaterialPageRoute(builder: (context) => SignUpScreen())
            );
          },
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(30)),
              border: Border.all(color: Colors.red, width: 2),
            ),
            width: size.width,
            height: 45,
            child: Center(
              child: Text("Register", style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold))
            ),
          ),
        ),
      ],
    );
  }

}