import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:berkah_manis_makmur/bloc/bloc_home.dart';
import 'package:berkah_manis_makmur/model/model_home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class HomeDetailScreen extends StatefulWidget {
  final String? assetCode;
  final List<MedalRespon>? model;
  
  const HomeDetailScreen({ Key? key, this.model, this.assetCode }) : super(key: key);

  @override
  _HomeDetailScreenState createState() => _HomeDetailScreenState();
}

class _HomeDetailScreenState extends State<HomeDetailScreen> {
  
  var bloc = BlocHome();
  XFile? imageFile;
  final ImagePicker _picker = ImagePicker();

  @override
  void initState() {
    super.initState();
    // bloc.getData(context);
  }

  @override
  void dispose() {
    super.dispose();
    bloc.dispose();
  }

  void cekPermission(BuildContext context, type) async {
    var status = await Permission.camera.status;
    if (status.isGranted) {
      if(type == "camera"){
        _openCamera(context);
      }else{
        _openGallery(context);
      }
    } else if (status.isDenied) {
      await Permission.camera.request();      
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: Text('Camera Permission'),
          content: Text(
              'This app needs camera access to take pictures'),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text('Deny'),
              onPressed: () => Navigator.of(context).pop(),
            ),
            CupertinoDialogAction(
              child: Text('Settings'),
              onPressed: () => openAppSettings(),
            ),
          ],
        ),
      );
    }
  }

  _openCamera(BuildContext context) async{
    var picture = await _picker.pickImage(source: ImageSource.camera);    
    this.setState(() {
      imageFile = picture;
      _encodeBase64(imageFile?.path);
    });
    Navigator.of(context);
  }

  _openGallery(BuildContext context) async{
    var picture = await _picker.pickImage(source: ImageSource.gallery);
    this.setState(() {
      imageFile = picture;
      _encodeBase64(imageFile?.path);
    });
    Navigator.of(context);
  }

  _encodeBase64(path){
    if(path != null){
      File file = File(path);
      print('File is = ' + file.toString());
      List<int> fileInByte = file.readAsBytesSync();
      String fileInBase64 = base64Encode(fileInByte);
      print("decode "+fileInBase64.toString());
      bloc.updateImage(context, widget.assetCode, fileInBase64);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('List Detail ${widget.assetCode.toString()}'),
      ),
      body: Container(
        child: Column(
          children: [
            streamGrid(),
            // showImageView(),
            // SizedBox(height: 30,),
            btnUploadImg()
          ],
        ),
      ),     
    );
  }

  Widget showImageView(){
    if(imageFile == null){
      return Text("No Images Seleceted!");
    }else{
     return Image.file(File(imageFile!.path), width: 400, height: 400,);
    }
  }

  Widget streamGrid(){
    print("stream");
    return Expanded(
      child: StreamBuilder(
        stream: bloc.getStreamData,
        builder: (context, AsyncSnapshot<List<MedalRespon>?> snapshot){
          if(snapshot.data != null){                    
            return dataTableView(snapshot.data);            
          }else if(widget.model!.length > 0){
            return dataTableView(widget.model);
          }else{
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        }
      ),
    );
  }

  Widget dataTableView(List<MedalRespon>? model){ 
    print("data");  
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 300,
      child: DataTable(
        headingRowColor: MaterialStateColor.resolveWith((states) => Colors.yellow),
        sortAscending: true,
        columns: const <DataColumn>[
          DataColumn(
            label: Text(
              'Name',
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
          ),
          DataColumn(
            label: Text(
              'Value',
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
          ),
        ],
        rows: model!
          .map((model) => new DataRow(
            cells: [
              DataCell(Text(model.name.toString())),
              DataCell(
                model.name == "Picture" 
                ? convertBase64(model.value)
                : 
                Text(model.value.toString())
              ),              
            ]))
          .toList()
        ),
    );
  }

  Widget convertBase64(value){
    Uint8List _bytesImage = Base64Decoder().convert(value.toString());
    return Image.memory(_bytesImage);
  }
  
  Widget btnUploadImg(){
    return Container(
      child: Column(
        children: [
          Text("UPLOAD IMAGE", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.green[700]),),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                    onPressed: (){
                      cekPermission(context, "camera");
                    },
                    child: Text("Open Camera", style: TextStyle(color: Colors.white),),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.green[700],
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      )
                    ),
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: ElevatedButton(
                    onPressed: (){
                      cekPermission(context, "gallery");
                    },
                    child: Text("Browse Gallery", style: TextStyle(color: Colors.white),),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.green[700],
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      )
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  

}