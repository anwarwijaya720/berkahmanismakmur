import 'dart:convert';
import 'dart:typed_data';
import 'package:berkah_manis_makmur/bloc/bloc_home.dart';
import 'package:berkah_manis_makmur/model/model_home.dart';
import 'package:berkah_manis_makmur/screen/drawer.dart';
import 'package:berkah_manis_makmur/screen/home_detail.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({ Key? key }) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  var bloc = BlocHome();
  late Size size;

  @override
  void initState() {
    super.initState();
    // bloc.getData(context);
  }

  @override
  void dispose() {
    super.dispose();
    // bloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
      ),
      drawer: Drawer(
        elevation: 20.0,
        child: HomeDrawer(),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: size.width,
          height: size.height,
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              cardBoxSearch(),
              // SizedBox(height: 20,),
              // streamList(),
            ],
          ),
        ),
      ),
    );
  }

  Widget cardBoxSearch(){
    return Column(
      children: [
        Text("Input Asset Code :"),
        SizedBox(height: 10,),
        TextFormField(
          keyboardType: TextInputType.text,
          controller: bloc.assetCodeController,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.only(top: 20),                        
            hintText: 'Search',
            errorStyle: TextStyle(color: Colors.red),
            prefixIcon: Icon(Icons.search),
          ),
        ),
        SizedBox(height: 10,),
        Container(
          width: MediaQuery.of(context).size.width,
          height: 45,
          child: ElevatedButton(
            onPressed: (){
              if(bloc.assetCodeController.text.length > 3){                
                bloc.getData(context);
              }
            },
            child: Text("Search"),
            style: ElevatedButton.styleFrom(
              primary: Colors.red,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
              )
            ),
          ),
        ),
      ],
    );
  }

  Widget streamList(){
    return Expanded(
      child: StreamBuilder(
        stream: bloc.getStreamData,
        builder: (context, AsyncSnapshot<List<MedalRespon>?> snapshot){
          if(snapshot.data != null){                    
            return listViewData(snapshot.data);            
          }else{
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        }
      ),
    );
  }

  Widget listViewData(List<MedalRespon>? model){
    return ListView.builder(
      itemCount: model!.length,
      itemBuilder: (context, index){        
        return showDataList(model[index]);        
      }
    );
  }

  Widget showDataList (MedalRespon model){    
    Uint8List? _bytesImage;
    if(model.name == "Picture"){
      _bytesImage = Base64Decoder().convert(model.value.toString());
      // _bytesImage = base64.decode(model.value.toString());
    }
    return Container(
      width: size.width,
      padding: EdgeInsets.only(bottom: 10.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(model.name.toString()),
                  Container(
                    width: size.width * 0.80,
                    child: 
                      model.name == "Picture" 
                      ?
                        Image.memory(_bytesImage!)
                      : 
                        Text(model.value.toString())
                  ),
                ],
              ),
              Icon(Icons.keyboard_arrow_right)
            ],
          ),
          Divider(),
        ],
      ),
    );
  }

}