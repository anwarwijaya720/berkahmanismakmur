import 'package:berkah_manis_makmur/screen/login.dart';
import 'package:berkah_manis_makmur/screen/user.dart';
import 'package:flutter/material.dart';

class HomeDrawer extends StatelessWidget {
  const HomeDrawer({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return 
     Container(
      height: MediaQuery.of(context).size.height,
      child: Column( 
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: <Color>[                  
                  Colors.cyan,
                  Colors.indigo,
                ]
              )
            ),
            child: Container(
              child: Row(
                children: <Widget>[
                  Container(
                    width: 70,
                    height: 70,
                    decoration: BoxDecoration(
                      color: Colors.grey,
                      borderRadius: BorderRadius.circular(50.0)
                    ),
                    child: Icon(Icons.person, size: 50, color: Colors.white,),
                  ),
                  Padding(padding: EdgeInsets.only(left: 10.0)),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('Anwar Wijya', style: infoTextStytle(),),
                      Text('punyasaya@gmail.com', style: infoTextStytle(),),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Card(
            elevation: 5,
            child: ListTile(
              leading: Icon(Icons.home),
              trailing: Icon(Icons.keyboard_arrow_right),
              title: Text('Home'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ),
          Card(
            elevation: 5,
            child: ListTile(
              leading: Icon(Icons.person),
              trailing: Icon(Icons.keyboard_arrow_right),
              title: Text('User'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => UserScreen()),
                ); 
              },
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          Card(
            elevation: 5,
            child: ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Logout'),
              onTap: () {                      
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                );
              },
            ),
          ), 
        ],
      ),
    );
  }

  static TextStyle infoTextStytle() {
    return TextStyle(
      color: Colors.white,
      fontSize: 12.0,
      fontWeight: FontWeight.bold,
    );
  }

}