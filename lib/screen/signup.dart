import 'package:berkah_manis_makmur/bloc/bloc_login.dart';
import 'package:flutter/material.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({ Key? key }) : super(key: key);

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {

  var bloc = LoginBloc();
  late Size size;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Signup"),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            tvUsername(),
            SizedBox(height: 10,),
            tvPassword(),
            SizedBox(height: 20,),
            btnSignup()
          ],
        ),
      ),
    );
  }

  Widget tvUsername(){
    return StreamBuilder<String>(
      stream: bloc.username,
      builder: (context, snapshot) {
        return TextFormField(
          controller: bloc.txtUsernameController,
          keyboardType: TextInputType.name,
          decoration: InputDecoration(
            icon: Icon(Icons.person),
            hintText: 'enter username',
            labelText: 'Username',
            errorText: snapshot.error?.toString(),
            errorStyle: TextStyle(color: Colors.red),
          ),
          onChanged: bloc.setUsername,
        );
      }
    );
  }

  Widget tvPassword(){
    return StreamBuilder<String>(
      stream: bloc.password,
      builder: (context, snapshot) {
        return TextFormField(
          controller: bloc.txtPasswordController,
          keyboardType: TextInputType.visiblePassword,
          decoration: InputDecoration(
            icon: Icon(Icons.lock),
            hintText: 'enter password',
            labelText: 'Passord',
            errorText: snapshot.error?.toString(),
            errorStyle: TextStyle(color: Colors.red),
          ),
          onChanged: bloc.setPassword,
        );
      }
    );
  }

  Widget btnSignup(){
    return StreamBuilder<bool>(
      stream: bloc.loginValid,
      builder: (context, snapshot) {
        return Padding(
          padding: EdgeInsets.only(top: 20.0),
            child: SizedBox(
            width: MediaQuery.of(context).size.width * 0.70,
            height: 45.0,
            child: ElevatedButton(
              child: Text("Submit", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
              style: ElevatedButton.styleFrom(
                primary: !snapshot.hasData ? Colors.grey : Colors.red,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30),
                ),  
              ),
              onPressed: (){
                snapshot.hasData ? bloc.fetchSqliteSave(context) : null ;
              },
            ),
          ),
        );
      }
    );
  }
}