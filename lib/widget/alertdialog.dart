import 'package:flutter/material.dart';

class CustomDialog {  

  alertDialog(BuildContext context, _title) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(_title.toString()),
          // content: new Text("Alert Dialog body"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),

            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),

          ],
        );
      },
    );
  }

  showAlertDialog(BuildContext context, txtTitle, txtcontent) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {         
        // Navigator.of(context).pop();
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(txtTitle ?? ''),
      content: Text(txtcontent ?? ''),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

}
