import 'package:flutter/material.dart';

class ProgressBar {
  late OverlayEntry _progressOverlayEntry;

  void show(BuildContext context) async {
    _progressOverlayEntry = _createdProgressEntry(context);
    Overlay.of(context)?.insert(_progressOverlayEntry);
  }

  void hide()async{
    if (_progressOverlayEntry != null) {      
    await new Future.delayed(Duration(seconds: 2));
      _progressOverlayEntry.remove();
      _progressOverlayEntry == null;
    }
  }

  OverlayEntry _createdProgressEntry(BuildContext context) =>
    OverlayEntry(builder: (BuildContext context) {
      return Opacity(
        opacity: 0.7,
        child: Container(
        color: Colors.black,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(color: Colors.white),
            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Text(
                "Loading...",
                style: TextStyle(color: Colors.white, fontSize: 13.0, decoration: TextDecoration.none),
              ))
          ],
        )),
      );
    }
  );

}
