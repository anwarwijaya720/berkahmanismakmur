class ModelDbLogin {

  final String? userId;
  final String? username;
  final String? password;

  ModelDbLogin({    
    this.userId, 
    this.username, 
    this.password
  });

}