class MedalRespon {
  String? name;
  String? datatype;
  String? value;  

  MedalRespon({this.name, this.datatype, this.value});

  factory MedalRespon.fromJson(Map<String, dynamic> json) {
    return MedalRespon(
      name : json['Name'] != null ? json['Name'] as String : null,
      datatype : json['DataType'] != null ? json['DataType'] as String : null,
      value : json['Value'] != null ? json['Value'] as String : null,
    );
  }
}

class HomeModel {

  final String? result;
  final String? message;
  final List<MedalRespon>? data;

  HomeModel({this.result, this.message, this.data});

  factory HomeModel.fromJson(Map<String, dynamic> json) {
    return HomeModel(
      result : json['result'] != null ? json['result'] as String : null,
      message : json['message'] != null ? json['message'] as String : null,
      data : json['data'] != null ? (json['data'] as List).map((i) => new MedalRespon.fromJson(i)).toList() : null,
    );
  }

}