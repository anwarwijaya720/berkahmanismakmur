import 'package:berkah_manis_makmur/model/model_dblogin.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DBHelper {
  static const dbName = "login";

  Database? dblogin;

   Future<Database> initDb() async {
    String databasesPath = await getDatabasesPath();
    String dbPath = join(databasesPath, dbName);

    var database = await openDatabase(dbPath, version: 3, onCreate: populateDb);
    return database;
  }

  void populateDb(Database database, int version) async {
    await database.execute("CREATE TABLE login("
        "id INTEGER PRIMARY KEY,"
        "user_id Text,"
        "username Text,"
        "password Text"
      ")");
  }

  Future<Database?> get database async {
    if (dblogin == null) {
      dblogin = await initDb();
    }
    return dblogin;
  }

  //select
  Future<List<ModelDbLogin>> select() async {
    Database? db = await this.database;
    var sql = "SELECT * FROM login ORDER BY username ASC";
    var data = await db!.rawQuery(sql);
    List<ModelDbLogin> modelCart = [];
    for (final node in data) {
      try {
        var model = ModelDbLogin(
          userId: node['user_id'] as String,
          username: node['username'] as String,
          password: node['password'] as String
        );
        modelCart.add(model);
      } catch (error) {
        print('select data fialed');
        print(error);
      }
    }
    return modelCart;
  }

  //select where
  Future<List<ModelDbLogin>> selectwhere(value) async {    
    Database? db = await this.database;
    var sql = "SELECT * FROM login WHERE username = '${value.toString()}' ";
    var data = await db!.rawQuery(sql);
    List<ModelDbLogin> modelCart = [];
    for (final node in data) {
      try {
        var model = ModelDbLogin(
          userId: node['user_id'] as String,
          username: node['username'] as String,
          password: node['password'] as String
        );
        modelCart.add(model);
      } catch (error) {
        print('get data fialed');
        print(error);
      }
    }
    return modelCart;
  }

  //insert data
  Future<int> insert(ModelDbLogin model) async {
    Database? db = await this.database;
    int count = await db!.insert(
      'login',
      {
        'user_id': model.userId,
        'username': model.username,
        'password': model.password
      },
    );
    return count;
  }

  //update data
  Future<int> update(ModelDbLogin model) async {
    Database? db = await this.database;
    int count = await db!.update(
        'login',
        {
          'user_id': model.userId,
          'username': model.username,
          'password': model.password
        },
        where: 'user_id=?',
        whereArgs: [model.userId]);
    return count;
  }

  //delete where
  Future<int> delete(userId) async {
    Database? db = await this.database;
    int count = await db!.delete('login',
        where: 'user_id=?', whereArgs: [userId]);
    return count;
  }

}