import 'package:berkah_manis_makmur/model/model_home.dart';
import 'package:berkah_manis_makmur/screen/home_detail.dart';
import 'package:berkah_manis_makmur/service/service.dart';
import 'package:berkah_manis_makmur/widget/alertdialog.dart';
import 'package:berkah_manis_makmur/widget/loading_screen.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class BlocHome {

  var service = Service(); 
  var alert = CustomDialog(); 
  ProgressBar sendingMsgProgressBar = ProgressBar();
  var assetCodeController = TextEditingController();
  
  final _isLoadingSubject = BehaviorSubject<bool>.seeded(false);
  Stream<bool> get isLoading => _isLoadingSubject.stream;

  final _fetcherImages = BehaviorSubject<List<MedalRespon>?>();
  Stream<List<MedalRespon>?> get getStreamData => _fetcherImages.stream;

  getData(BuildContext context) async {
    sendingMsgProgressBar.show(context);
    HomeModel result = await service.serviceImages(assetCodeController.text.toString());     
    sendingMsgProgressBar.hide();
    if(result.data!.isNotEmpty){
      _fetcherImages.sink.add(result.data);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => HomeDetailScreen(assetCode: assetCodeController.text.toString(), model: result.data,)),
      );
    }else{
      showSnackBar(context, "there is an error");
    }
  }

  updateImage(BuildContext context, assetCode, imgBase64) async {
    sendingMsgProgressBar.show(context);
    HomeModel result = await service.serviceUpdateImages(assetCode, imgBase64);
    sendingMsgProgressBar.hide();
    print(result.message.toString());
    _fetcherImages.sink.add(result.data);
  }

  void dispose(){
    assetCodeController.dispose();
    _isLoadingSubject.close();
    _fetcherImages.close();
  }

  void showSnackBar(BuildContext context, String text) {
    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(SnackBar(content: Text(text)));
  }

}