import 'dart:async';
import 'package:berkah_manis_makmur/helper/db_helper.dart';
import 'package:berkah_manis_makmur/model/model_dblogin.dart';
import 'package:berkah_manis_makmur/screen/home.dart';
import 'package:berkah_manis_makmur/widget/alertdialog.dart';
import 'package:berkah_manis_makmur/widget/loading_screen.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:uuid/uuid.dart';

class LoginBloc {

  ProgressBar sendingMsgProgressBar = ProgressBar();
  var alert = CustomDialog();
  var uuid = Uuid();
  
  var txtUsernameController = TextEditingController();
  var txtPasswordController = TextEditingController();

  final _username = BehaviorSubject<String>();
  final _password = BehaviorSubject<String>();

  //Set
  Function(String) get setUsername => _username.sink.add;
  Function(String) get setPassword => _password.sink.add;

  //Get
  Stream<String> get username => _username.stream.transform(validationUsername);
  Stream<String> get password => _password.stream.transform(validationPassword);
  Stream<bool> get loginValid => Rx.combineLatest2(username, password, (username, password) => true);

  final _fetcherGetSqlite = BehaviorSubject<List<ModelDbLogin>>();
  Stream<List<ModelDbLogin>> get getDbSqliteUser => _fetcherGetSqlite.stream;

  bool Status = false;

  dispose(){
    _username.close();
    _password.close();
  }

  fetchSqliteGet(context) async {
    print('get sqlite');
    List<ModelDbLogin> result = await DBHelper().select();
    if (result.isNotEmpty) {
      print(result);
      return _fetcherGetSqlite.sink.add(result);
    } else {
      print("data sqlite empty");
    }
  }

  fetchSqliteValidateLogin(context) async {
    print('valdate login');
    List<ModelDbLogin> result = await DBHelper().selectwhere(txtUsernameController.text);    
    if (result.isNotEmpty) {      
      validateLogin(context);
      // return _fetcherGetSqlite.sink.add(result);
    } else {
      showSnackBar(context, "username not found");
      print("data sqlite empty");
    }
  }

  fetchSqliteSave(BuildContext context) async {

    List<ModelDbLogin> result = await DBHelper().selectwhere(txtUsernameController.text);   
    print(result);

    if(result.isEmpty){
      
      sendingMsgProgressBar.show(context);

      var model = ModelDbLogin(
          userId: uuid.v4(),
          username: txtUsernameController.text.toString(),
          password: txtPasswordController.text.toString()
        );

      var response = await DBHelper().insert(model);

      print(model.userId.toString());
      print(model.username.toString());
      print(model.password.toString());
      
      txtUsernameController.clear();
      txtPasswordController.clear();
      sendingMsgProgressBar.hide();

      print(response.toString());
      if (response > 0) {
        print('save data successfully');
        validateSignup(context);
      } else {
        print('process failed');
        alert.showAlertDialog(context, "Alert", "process failed");                
      }    
    }else{
      txtUsernameController.clear();
      txtPasswordController.clear();
      showSnackBar(context, "username already exists");
    }

  }

  //Transformers
  final validationUsername = StreamTransformer<String, String>.fromHandlers(
    handleData: (username,sink){
      if(username.length <= 2){
        sink.addError("Username must be at least 3 characters");        
      }else{
        sink.add(username);
      }
    }
  );

  final validationPassword = StreamTransformer<String, String>.fromHandlers(
    handleData: (password,sink){
      if(password.length <= 5){
        sink.addError("password must be at least 6 characters");
      }else{
        sink.add(password);
      }
    }
  );

  validateLogin(BuildContext context){ 
    txtUsernameController.clear();
    txtPasswordController.clear();   
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MyHomePage()),
    );     
  }

  validateSignup(BuildContext context){        
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MyHomePage()),
    );     
  } 

  void showSnackBar(BuildContext context, String text) {
    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(SnackBar(content: Text(text)));
  }
  
}