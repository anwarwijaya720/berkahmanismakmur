import 'dart:convert';
import 'package:berkah_manis_makmur/model/model_home.dart';
import 'package:http/http.dart' as http;
import 'dart:async';

class Service {
  
  var url = "http://api2.berkahmm.com:3100/testing/viewtest";

  Future<HomeModel> serviceImages(assetCode) async{
    print("aseet code "+assetCode.toString());
    Map<String, String> headers = {
      "content-type": "application/json",
    };
    var body = jsonEncode({"assetCode":"${assetCode.toString()}"});
    final response = await http.post(Uri.parse(url), headers: headers, body: body);
    print("url get :"+url);
    print(response.body);
    if (response.statusCode == 200) {
      print("response ${response.body}");
      var body = jsonDecode(response.body);      
      var result = HomeModel.fromJson(body);
      return result;
    } else {
      print("gagal");
      throw Exception('Failed to load booking');
    }
  }

  Future<HomeModel> serviceUpdateImages(assetCode, imgBase64) async{
    print("aseet code "+assetCode.toString());
    print("img base64 "+imgBase64.toString());
    Map<String, String> headers = {
      "content-type": "application/json",
    };
    var body = jsonEncode({
      "assetCode":"${assetCode.toString()}",
      "assetPicture":"${imgBase64.toString()}",
    });
    final response = await http.post(Uri.parse(url), headers: headers, body: body);
    print("url get :"+url);
    print(response.body);
    if (response.statusCode == 200) {
      print("response ${response.body}");
      var body = jsonDecode(response.body);      
      var result = HomeModel.fromJson(body);
      return result;
    } else {
      print("gagal");
      throw Exception('Failed to load booking');
    }
  }

}